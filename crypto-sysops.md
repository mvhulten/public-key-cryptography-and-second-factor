        PRACTICAL PUBLIC-KEY CRYPTOGRAPHY
\
        (C) 2021, Marco van Hulten, CC BY-SA

# TODO ALVORENS:
#  killall ssh-agent & opnieuwe inloggen

Contents:
⚫ Fundamentals
⚫ SSH key authentication
⚫ Pretty Good Privacy (PGP)
⚫ Two-factor authentication (2FA)

                              FUNDAMENTALS
\
Symmetric-key algorithm:
0. Create and communicate a secret key Ss (somehow).
1. Alice encrypts plaintext with Ss, and sends the ciphertext to Bob.
2. Bob receives and decrypts with Ss.
3. Both Bob and Alice keep the shared secret key Ss "secret".

@symmetric.png

Public-key algorithm:
0. Both parties create a keypair (Ap, As), (Bp, Bs) and share Ap and Bp.
1. Alice encrypts plaintext with Bp.
2. Bob receives and decrypts with Bs.
3. Alice keeps her As and Bob his Bs SECRET.

@asymmetric.png

Public-key algorithm advantages:
\
⚫ No shared secret (as opposed to symmetric key cryptography).
    ▸ Protect you secret keys.
    ▸ Share your public keys.

                         SSH KEY AUTHENTICATION
\
OpenSSH authentication methods:
⚫ Password.
⚫ Public-key cryptography without passphrase.
⚫ Public-key cryptography with passphrase.
\
Cryptographic key protocol (OpenSSH):
1. A secure (asymmetric) channel is established.
2. A session key is negotiated through this channel
   (Diffie–Hellman–Merkle key agreement).
3. The symmetric channel is used for further communication.
\
It is more efficient and a secure method to share a cryptovariable.

SSH DEMO: public-key authentication with passphrase
## NOT CREATING KEYS; LOOK AT Metacenter's interndokumentasjon/jump-hosts.md
## Ed25519: q=2^255-19
# ssh-agent -D      # REMOVE ANY REMNANT COOKIES!
# cd
# vim .ssh/config
# ssh login         # DON'T SHOW THE "WRONG KEYS" HERE IN .ssh/authorized_keys
# ssh petri         # SHOW LOCAL PROJECT (IS CLEANER)
# ssh hpcmaster3    # DOESN'T WORK
# vim bin/ssha      # AND THEN REPEAT ssh LIKE ABOVE
# vim .xsession
# ssh-add -L        # ON E.G. petri
#                   # AGAIN DO THE ssh SEQUENCE (can now also just 'ssh login')

                       PRETTY GOOD PRIVACY (PGP)
Two goals:
1. Sign data (your secret key needed)
2. Encrypt data (the others' public key needed)

GnuPG DEMO:
# Security := (1) keep private key secret (no other people have access);
#		  and (2) keep backups (ability to have access).
#
1. create GnuPG keypair
# gpg --list-keys
# gpg --quick-gen-key test2
#
2. encrypt and decrypt some data
# gpg -e file.txt			# AFTER MAKING A TEXT FILE
#							# e-mails must be in plain text; usually plaintext
# shred file.txt
# gpg -d file.txt.gpg
#
3. use case: Password Store
# ssh petri
# pass list
# cd password-stores/staff/
# git remote -v             # You were invited (GitLab)!
# sudo -i
# pass list
# logout
# pass generate Undeadly
#   user:mvhulten
#   url:https://undeadly.org/
# pass list Undeadly
# gpg --sign-key hpcteam
# ...

Use cases:
⚫ Password Store, pass(1)
⚫ Verify authenticity
⚫ E-mail confidential information

Web of trust
\
We need:
⚫ To verify identities
⚫ Key signing party

@party.png

#@party2.png

                     MULTI-FACTOR AUTHENTICATION
# START A tmux IN A root SHELL ON string!
\
Three types of factors:
\
Something only the user...
⚫ ...knowns (password, passphrase)
⚫ ...possesses (mobile phone, keycard)
⚫ ...is (finger print, iris scan)

Two approaches discussed here:
\
1. Time-based One-Time Password (TOTP)
2. Universal 2nd Factor (FIDO/U2F)
#   + public key cryptography!

Time-based One-Time Password (TOTP):
\
⚫ SMS:
    ▸ Telephony (SS7) routing flaws
    ▸ SIM hijacking
⚫ OTP app (on same device)
    ▸ No physical separation
#   ABN Amro said using a separate app on same device is «physical separation»
⚫ OTP on different device
    ▸ Shared secret!

@totp.png

TOTP DEMO: Time-based One-Time Password
in addition to password on OpenBSD
\
⚫ add user
⚫ add 2nd factor (TOTP)
# START A tmux IN A root SHELL ON string!
#
# adduser               # 'scuser'; default login class
# ssh scuser@localhost  # login with password
#$ logout
# pkg_add login_oath    # to add a new login class (not in base)
#---(C-B+")---
# less /usr/local/share/doc/pkg-readmes/login_oath
#---(C-B+;)---
# cd /etc/
# vi login.conf         # add TOTPPW (both TOTP and password)
#                       # mention all the interesting stuff that IS in base
# hg status
# userinfo scuser
# usermod -L totppw scuser
# userinfo scuser
# ssh scuser@localhost  # login with password FAILS
# su - scuser
#$ openssl rand -hex 20 > ~/.totp-key   # actually OpenBSD's fork LibreSSL
#$ chmod 400 .totp-key
#$ cat .totp-key        # it's okay, i've set a password
#                       # convert to base32 if using device
#$ logout
# oathtool --totp $(cat ~scuser/.totp-key) # from oath-toolkit
#$ ssh scuser@localhost # now enter TOTP_CODE/password
\
# To clean up:      OR: # sh clean-up.sh
# userdel -r scuser
# groupdel scuser
# pkg_delete login_oath
# hg revert login.conf

But how to exchange the secret (~/.totppw-key)?
\
Questions so far?
#
# In case of MS Teams you have to trust MS with
#  1. potentially your UiB password(!)
#  2. certainly with the shared (TOTP) secret.

Universal 2nd Factor (FIDO/U2F)
\
⚫ Public-key cryptography
    ▸ No shared secret

@u2f.png
# The device only sends your public key!
# You must have a hardware device (software U2F is stupid)

Implementations:
\
⚫ YubiKey ▸ proprietary
⚫ Trezor ▸ free
⚫ sofware U2F ▸ just stupid

Not discussed:
⚫ SSH key authentication + second factor
#⚫ New ssh key distribution scheme in Metacenter (with Ansible)
#    ▸ future goal is to include 2FA
# testing on NREC instances (Pavel?)

                              CONCLUSIONS / THESES
\
# commented out SSH/GnuPG specific stuff
#⚫ Public-key authentication is IMPORTANT and UBIQUITOUS.
#⚫ We should use SSH key authentication,
#    ▸ keeping the secret key safe, and
#    ▸ passphrase-protected.
⚫ We should send sensitive information only encrypted.
⚫ We should use public-key authentication, also for 2FA.
⚫ 2FA can improve security:
    ▸ TOTP can be OK, but
    ▸ U2F is better.

Questions?

                              BIBLIOGRAPHY
\
⚫ SSH, GnuPG
    https://documentation.sigma2.no/getting_started/create_ssh_keys.html
    https://www.passwordstore.org/
\
⚫ 2FA
    https://dataswamp.org/~solene/2021-02-06-openbsd-2fa.html
    https://openauthentication.org/
 ▸ U2F
    https://blog.trezor.io/why-you-should-never-use-google-authenticator-again-e166d09d4324
    https://www.yubico.com/authentication-standards/fido-u2f/
    https://fidoalliance.org/

